--- Required libraries.
local Container = require( ( _G.scrappyDir or "" ) .. "data.container" )

-- Localised functions.
local json = require( "json" )
local mime
if system.getInfo( "platform" ) == "html5" or system.getInfo( "platform" ) == "nx64" then
	local base64 = require( ( _G.scrappyDir or "" ) .. "data.base64" )
	mime =
	{
		b64 = base64.encode,
		unb64 = base64.decode
	}
else
	mime = require( "mime" )
end

-- Localised values.
local encode = json.encode
local decode = json.decode
local b64 = mime.b64
local unb64 = mime.unb64

--- Class creation.
local library = {}

library._containers = {}
library._primary = nil

--- Loads a container.
-- @param name The name of the container.
-- @return The new container.
function library:loadContainer( name )

	if self._containers[ name ] then
		if not self._containers[ name ]._loaded then
			self._containers[ name ]:load()
		end
		return self._containers[ name ]
	else

		local container = Container:new( name )

		self._containers[ name ] = container

		return container

	end

end

--- Gets a container.
-- @param name The name of the container.
-- @return The container.
function library:getContainer( name )
	return self._containers[ name ]
end

--- Exports a container into base 64 data.
-- @param name The name of the container to export. Optional, defaults to the primary one.
-- @return The encoded data.
function library:exportContainer( name )

	-- Get the actual container object
	local container = self:loadContainer( name or "scrappy" )

	-- Get the names of all the boxes in the old container
	local names = container:list()

	-- Table to store the boxes
	local boxes = {}

	-- Loop through the names
	for i = 1, #names, 1 do

		-- Export the box data
		boxes[ names[ i ] ] = container:export( names[ i ] )

	end

	-- Json encode the data
	local data = encode( boxes )

	-- Base 64 encode the data
	data = b64( data )

	-- Return the data
	return data

end

--- Imports data from an exported container into another container.
-- @param data The data to import.
-- @param name The name of the container to import this into. Optional, defaults to the primary one.
-- @param params The params for initialising the new container.
-- @return The imported container.
function library:importContainer( data, name, params )

	-- Get the actual container object
	local container = self:loadContainer( name or "scrappy" )

	-- Initialise the container
	if not container:isInitiated() then
		container:init( params )
	end

	-- Base 64 decode the data
	data = unb64( data )

	-- Json decode the data
	local boxes = decode( data )

	-- Loop through the boxes
	for k, v in pairs( boxes ) do

		-- Import the data, passing in the box name
		container:import( v, k )

	end

	-- Save out the new container
	container:save()

	-- Return the container
	return container

end

--- Duplicates a container.
-- @param name The name of the new container.
-- @param container The name of the container to duplicate. Optional, defaults to the primary one.
-- @return The new container.
function library:duplicateContainer( name, container )

	-- Do we have a name for the new container?
	if name then

		-- Get the current data
		local data = self:exportContainer( container )

		-- Do we have data?
		if data then

			local params = self:getContainer( container or "scrappy" )._params

			-- Import the data into a new container
			local new = self:importContainer( data, name, params )

			-- Store the container
			self._containers[ name ] = new

			-- And return it
			return new

		end

	end

end

-- If we don't have a global Scrappy object i.e. this is the first Scrappy plugin to be included
if not Scrappy then

	-- Create one
	Scrappy = {}

end

-- If we don't have a Scrappy Data library
if not Scrappy.Data then

	-- Then store the library out
	Scrappy.Data = library

end

-- Do we not have our primary container?
if not library._primary then

	-- Create the primary container and store it out
	library._primary = library:loadContainer( "scrappy" )

end

-- Create all the passthrough functions for the primary container
function library:init( ... ) return library._primary:init( ... ) end
function library:get( ... ) return library._primary:get( ... ) end
function library:set( ... ) return library._primary:set( ... ) end
function library:setIfNew( ... ) return library._primary:setIfNew( ... ) end
function library:is( ... ) return library._primary:is( ... ) end
function library:isHigher( ... ) return library._primary:isHigher( ... ) end
function library:isLower( ... ) return library._primary:isLower( ... ) end
function library:isSet( ... ) return library._primary:isSet( ... ) end
function library:increment( ... ) return library._primary:increment( ... ) end
function library:decrement( ... ) return library._primary:decrement( ... ) end
function library:isNumber( ... ) return library._primary:isNumber( ... ) end
function library:isString( ... ) return library._primary:isString( ... ) end
function library:isTable( ... ) return library._primary:isTable( ... ) end
function library:import( ... ) return library._primary:import( ... ) end
function library:export( ... ) return library._primary:export( ... ) end
function library:duplicate( ... ) return library._primary:duplicate( ... ) end
function library:save( ... ) return library._primary:save( ... ) end
function library:load( ... ) return library._primary:load( ... ) end
function library:wipe( ... ) return library._primary:wipe( ... ) end
function library:delete( ... ) return library._primary:delete( ... ) end
function library:exists( ... ) return library._primary:exists( ... ) end
function library:destroy( ... ) return library._primary:destroy( ... ) end
function library:enableAutosave( ... ) return library._primary:enableAutosave( ... ) end
function library:disableAutosave( ... ) return library._primary:disableAutosave( ... ) end
function library:isAutosaveEnabled( ... ) return library._primary:isAutosaveEnabled( ... ) end
function library:enableSaveOnSuspend( ... ) return library._primary:enableSaveOnSuspend( ... ) end
function library:disableSaveOnSuspend( ... ) return library._primary:disableSaveOnSuspend( ... ) end
function library:isSavingOnSuspendEnabled( ... ) return library._primary:isSavingOnSuspendEnabled( ... ) end
function library:enableSaveOnExit( ... ) return library._primary:enableSaveOnExit( ... ) end
function library:disableSaveOnExit( ... ) return library._primary:disableSaveOnExit( ... ) end
function library:isSavingOnExitEnabled( ... ) return library._primary:isSavingOnExitEnabled( ... ) end
function library:enableLog( ... ) return library._primary:enableLog( ... ) end
function library:disableLog( ... ) return library._primary:disableLog( ... ) end
function library:isLogEnabled( ... ) return library._primary:isLogEnabled( ... ) end
function library:isInitiated( ... ) return library._primary:isInitiated( ... ) end
function library:isSaving( ... ) return library._primary:isSaving( ... ) end
function library:isLoading( ... ) return library._primary:isLoading( ... ) end
function library:print( ... ) return library._primary:print( ... ) end
function library:clearLog( ... ) return library._primary:clearLog( ... ) end
function library:list( ... ) return library._primary:list( ... ) end
function library:hash( ... ) return library._primary:hash( ... ) end
function library:authenticate( ... ) return library._primary:authenticate( ... ) end
function library:getVersion( ... ) return library._primary:getVersion( ... ) end

-- Return the new library
return library
