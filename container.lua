-- Middleclass library.
local class = require( ( _G.scrappyDir or "" ) .. "data.middleclass" )

--- Class creation.
local Container = class( "Container" )

--- Required libraries.
local lfs = require( "lfs" )
local json = require( "json" )
local crypto = require( "crypto" )
local mime
if system.getInfo( "platform" ) == "html5" or system.getInfo( "platform" ) == "nx64" then
	local base64 = require( ( _G.scrappyDir or "" ) .. "data.base64" )
	mime =
	{
		b64 = base64.encode,
		unb64 = base64.decode
	}
else
	mime = require( "mime" )
end

local openssl

-- Localised functions.
local find = string.find
local char = string.char
local gsub = string.gsub
local format = string.format
local pathForFile = system.pathForFile
local dir = lfs.dir
local open = io.open
local close = io.close
local delete = os.remove
local date = os.date
local time = os.time
local clock = os.clock
local encode = json.encode
local decode = json.decode
local print = print
local digest = crypto.digest
local hmac = crypto.hmac
local yield = coroutine.yield
local wrap = coroutine.wrap
local performWithDelay = timer.performWithDelay
local cancel = timer.cancel
local b64 = mime.b64
local unb64 = mime.unb64
local concat = table.concat
local min = math.min
local remove = table.remove

-- Localised values.
local DocumentsDirectory = system.DocumentsDirectory
local md5 = crypto.md5

--- Creates a new Container object.
-- @param name The name of the new container.
-- @return The new scrappy data container.
function Container:initialize( name )

	-- New object
	local container = {}

	-- Version number
	self._version = 0.1

	-- Table to store all data
	self._data = {}

	-- Set the name of this container
	self._name = name

	-- Name of the default box
	self._defaultBox = "default"

	-- Extension used for container files
	self._extension = ".scrappyData"

	-- Filename used for the data file
	self._datafile = pathForFile( self._name .. self._extension, DocumentsDirectory )

	-- Filename used for the log file
	self._logfile = pathForFile( "scrappy.log", DocumentsDirectory )

	-- Flag to mark if we're initiated or not
	self._initiated = false

	-- Table to store all custom event listeners
	self._listeners = {}

	-- Register for system events.
	Runtime:addEventListener( "system", self )

end

--- Initiates this container.
-- @param params The params for the new container.
-- @return The container.
function Container:init( params )

	-- Get the initiation params
	self._params = params or {}

	-- Should we enable encryption
	if self._params.key then

		-- Store out the key
		self._key = self._params.key

		-- Mark the original key as hidden
		self._params.key = "************"

		-- We may have a key but we may still want to disable encryption if we're only using the key for hashing
		self._disableEncryption = self._params.disableEncryption

	end

	-- Store out the chunk size, if we're using them
	self._chunkSize = self._params.chunkSize

	-- Should all data saving be done in plaintext?
	self._plaintext = self._params.plaintext

	-- Should we clear the log
	if self._params.clearLog then
		self:clearLog()
	end

	-- Should we enable logging
	if self._params.logging then
		self:enableLog()
	end

	-- Log the initiation
	self:_log( "begin initiation - " .. encode( self._params ), "init" )

	-- Should we enable autosave
	if self._params.autosave then
		self:enableAutosave()
	end

	-- Should we enable saving on suspend
	if self._params.saveOnSuspend then
		self:enableSaveOnSuspend()
	end

	-- Should we enable saving on exit
	if self._params.saveOnExit then
		self:enableSaveOnExit()
	end

	-- Do we have a listener?
	if self._params.listener then
		self._listener = self._params.listener
	end

	-- Should we enable encryption
	if self._key and not self._disableEncryption then

		-- Do a safe call to see if we can include the plugin
		local success, err = pcall( require, "plugin.openssl" )

	    -- Require the ssl plugin
		if success then
		    openssl = require( "plugin.openssl" )
		end

		-- Do we have the ssl plugin
		if openssl then

			-- Set the encryption suite
			self._ciperSuite = "aes-256-cbc"

			-- Create the cipher
			self._cipher = openssl.get_cipher( self._ciperSuite )

		else

			-- Nil out the key
			self._key = nil

			-- No plugin
			self:_log( "openssl plugin required for encryption.", "init" )

		end

	end

	-- Load up all stored data
	self:load()

	-- If we don't have a default box, create one now
	if not self._data[ self._defaultBox ] then
		self:_create( self._defaultBox )
	end

	-- Mark us as initiated
	self._initiated = true

	-- Log the initiation
	self:_log( "initiated.", "init" )

end

--- Create a new data storage box.
-- @param name The name of the box.
function Container:_create( name )


	-- Do we have a name?
	if name or self._defaultBox then

		-- Do we already have a box named that?
		if self._data[ ( name or self._defaultBox ) ] then

		else

			-- Create some blank data
			local data =
			{

			}

			-- Create the header for the data
			local header =
			{
				version = self._version,
				name = ( name or self._defaultBox ),
				created = time()
			}

			-- Create an empty box
			self._data[ ( name or self._defaultBox ) ] =
			{
				data = data,
				header = header
			}

			self._data[ ( name or self._defaultBox ) ].header.checksum = self:hash( name, self._key )

			-- Log the creation
			self:_log( ( name or self._defaultBox ) .. " created.", "_create" )

		end

	else

		-- A name must be passed in
		self:_log( "Can not create box, no name passed in.", "_create" )

	end

end

--- Gets a value.
-- @param name The name of the value to get.
-- @param box The name of the box. Optional, defaults to the global box.
-- @return The value.
function Container:get( name, box )

	-- Have we loaded correctly?
	if self._loaded then

		-- Create a new box if needed
		self:_create( box or self._defaultBox )

		-- Check we have a box
		if self._data[ box or self._defaultBox ] then

			-- Return the value
			return self._data[ box or self._defaultBox ].data[ name ]

		end

	end

end

--- Gets all values.
-- @param box The name of the box. Optional, defaults to the global box.
-- @return The value.
function Container:getAll( box )

	-- Have we loaded correctly?
	if self._loaded then

		-- Create a new box if needed
		self:_create( box or self._defaultBox )

		-- Check we have a box
		if self._data[ box or self._defaultBox ] then

			-- Return the value
			return self._data[ box or self._defaultBox ].data

		end

	end

end

--- Sets a value.
-- @param name The name of the value to set.
-- @param value The new value.
-- @param box The name of the box. Optional, defaults to the global box.
function Container:set( name, value, box )

	-- Have we loaded correctly?
	if self._loaded then

		-- Create a new box if needed
		self:_create( box or self._defaultBox )

		-- Check we have a box
		if self._data[ box or self._defaultBox ] then

			-- Fire an event!
			self:dispatchEvent{ name = "set", key = name, old = self:get( name, box ), new = value, box = box }

			-- Set the value
			self._data[ box or self._defaultBox ].data[ name ] = value

			-- Do we have a header
			if self._data[ box or self._defaultBox ].header then

				-- Set the modified time
				self._data[ box or self._defaultBox ].header.modified = time()

				self._data[ box or self._defaultBox ].header.checksum = self:hash( box, self._key )

			end

			-- Should we now save?
			if self:isAutosaveEnabled() then
				self:save( box or self._defaultBox )
			end

		end

	end

end

--- Sets a value if it isn't currently set.
-- @param name The name of the value to set.
-- @param value The new value.
-- @param box The name of the box. Optional, defaults to the global box.
function Container:setIfNew( name, value, box )

	-- Have we loaded correctly?
	if self._loaded then

		-- Create a new box if needed
		self:_create( box or self._defaultBox )

		-- Check we have a box
		if self._data[ box or self._defaultBox ] then

			-- Is the current value nil? i.e. not set
			if self:is( name, nil, ( box or self._defaultBox ) ) then

				-- Set the value
				self:set( name, value, box )

				-- Should we now save?
				if self:isAutosaveEnabled() then
					self:save( box or self._defaultBox )
				end

			end

		end

	end

end

--- Checks if a value is equal to another value.
-- @param name The name of the value to check.
-- @param value The other value.
-- @param box The name of the box. Optional, defaults to the global box.
-- @return True if it is, false if not.
function Container:is( name, value, box )
	return self:get( name, box ) == value
end

--- Checks if a number value is higher than another value.
-- @param name The name of the value to check.
-- @param value The other value.
-- @param box The name of the box. Optional, defaults to the global box.
-- @return True if it is, false if not.
function Container:isHigher( name, value, box )
	return self:get( name, box ) > value
end

--- Checks if a number value is lower than another value.
-- @param name The name of the value to check.
-- @param value The other value.
-- @param box The name of the box. Optional, defaults to the global box.
-- @return True if it is, false if not.
function Container:isLower( name, value, box )
	return self:get( name, box ) < value
end

--- Checks if a value has been set.
-- @param name The name of the value to check.
-- @param box The name of the box. Optional, defaults to the global box.
-- @return True if it has been, false if not.
function Container:isSet( name, box )
	return self:get( name, box ) ~= nil
end

--- Increments a number value.
-- @param name The name of the value to increment.
-- @param amount The amount to increment by. Optional, defaults to 1.
-- @param box The name of the box. Optional, defaults to the global box.
function Container:increment( name, amount, box )
	if self:isNumber( name, box ) then
		self:set( name, self:get( name, box ) + ( amount or 1 ), box )
	end
end

--- Decrements a number value.
-- @param name The name of the value to decrement.
-- @param amount The amount to decrement by. Optional, defaults to 1.
-- @param box The name of the box. Optional, defaults to the global box.
function Container:decrement( name, amount, box )
	if self:isNumber( name, box ) then
		self:set( name, self:get( name, box ) - ( amount or 1 ), box )
	end
end

--- Checks if a value is a number.
-- @param name The name of the value to check.
-- @param box The name of the box. Optional, defaults to the global box.
-- @return True if it is, false if not.
function Container:isNumber( name, box )
	return type( self:get( name, box ) ) == "number"
end

--- Checks if a value is a string.
-- @param name The name of the value to check.
-- @param box The name of the box. Optional, defaults to the global box.
-- @return True if it is, false if not.
function Container:isString( name, box )
	return type( self:get( name, box ) ) == "string"
end

--- Checks if a value is a table.
-- @param name The name of the value to check.
-- @param box The name of the box. Optional, defaults to the global box.
-- @return True if it is, false if not.
function Container:isTable( name, box )
	return type( self:get( name, box ) ) == "table"
end

--- Imports a data box.
-- @param data The exported data to import, in b64 encoding.
-- @param box The name of the box. Optional, defaults to the global box.
function Container:import( data, box )

	-- Get the current time for before the call
	local t1 = clock()

	-- Fire an event!
	self:dispatchEvent{ name = "import", phase = "started", start = t1 }

	-- Binary decode the data
	data = self:b64Decode( data )

	-- Do we actually have some date?
	if data then

		-- Decode the data
		data = decode( data )

	end

	-- Was the data decoded properly
	if data then

		-- Json decode the data and store it
		self._data[ box or self._defaultBox ] = data

		-- Should we now save?
		if self:isAutosaveEnabled() then
			self:save()
		end

		-- Fire an event!
		self:dispatchEvent{ name = "import", phase = "ended", start = t1, finish = clock(), elapsed = self:_elapsed( t1 ), data = data }

		-- Return successful
		return true

	end

end

--- Exports a data box.
-- @param box The name of the box. Optional, defaults to the global box.
-- @return The exported data, in b64 encoding. Nil if none found.
function Container:export( box )

	-- Get the data
	local data = self._data[ box or self._defaultBox ]

	-- Do we have some data?
	if data then

		-- Get the current time for before the call
		local t1 = clock()

		-- Fire an event!
		self:dispatchEvent{ name = "export", phase = "started", start = t1 }

		-- Json encode the data
		data = encode( data )

		-- Binary encode the data
		data = self:b64Encode( data )

		-- Fire an event!
		self:dispatchEvent{ name = "export", phase = "ended", start = t1, finish = clock(), elapsed = self:_elapsed( t1 ) }

		-- Return the encoded data
		return data

	end

end

--- Duplicates a box into a newly created one.
-- @param name The name of the new box.
-- @param box The name of the box to duplicate. Optional, defaults to the global box.
function Container:duplicate( name, box )

	-- Do we have a name for the new box?
	if name then

		-- Get the current data
		local data = self:export( box )

		-- Do we have data?
		if data then
			self:import( data, name )
		else
			-- Log the error
			self:_log( "Error: no data found for duplication.", "duplicate" )
		end

	else

		-- Log the error
		self:_log( "Error: name of the new box must be set.", "duplicate" )

	end

end

--- Saves all data.
function Container:save()

	-- Ensure we aren't currently saving
	if not self:isSaving() then

		-- Have we loaded correctly?
		if self._loaded then

			-- Open the file for writing
			local file, errorString = open( self._datafile, "wb" )

			-- Do we have a file?
			if file then

				-- Close the file
				close( file )

				-- Get the current time for before the call
				local t1 = clock()

				-- Mark us as saving
				self._isSaving = true

				-- Fire an event!
				self:dispatchEvent{ name = "save", phase = "started", start = t1 }

				-- Get the data
				local data = self._data

				if type( data ) == "table" then

					-- Json encode the data
					data = encode( data )

					-- Create a chunks table with one single chunk, the whole data
					local chunks = { data }

					-- Are we actually using chunks?
					if self._chunkSize then

						-- Then split the data into bite sized chunks
						chunks = self:_chunk( data, self._chunkSize )

					end

					-- Binary Enccode the data
					if not self._plaintext then

						-- Forward declare a timer id
						local timerID

						-- Function for encoding the data
						local encode = function()

							-- Loop through each chunk
							for i = 1, #chunks, 1 do

								-- Encode each chunk
								chunks[ i ] = self:b64Encode( chunks[ i ] )

								-- Are we using chunks?
								if self._chunkSize then

									-- Yield the coroutine
									yield()

								end

					        end

							-- Cancel the timer
							if timerID then
								cancel( timerID )
							end

							-- Nil out the handle
							timerID = nil

							-- Clear out the data
							data = ""

							-- Loop through the encoded chunks
							for i = 1, #chunks, 1 do

								-- And append them to the data
								data = data .. chunks[ i ]

							end

						end

						-- Are we using chunks?
						if self._chunkSize then

							-- If so then create a timer and wrap the encode function up as a coroutine
							timerID = performWithDelay( 1, wrap( encode ), 0 )

						else

							-- Otherwise just encode as normal
							encode()

						end

					end

					-- Do we have a cipher?
					if self._cipher and not self._plaintext and not self._disableEncryption then

						-- Encrypt the data
						data = self._cipher:encrypt( data, self._key )

					end

					-- Create a chunks table with one single chunk, the whole data
					chunks = { data }

					-- Are we actually using chunks?
					if self._chunkSize then

						-- Then split the data into bite sized chunks
						chunks = self:_chunk( data, self._chunkSize )

					end

					-- Forward declare a timer id
					local timerID

					-- Function for writing out the data to file
					local write = function()

						-- Loop through each chunk
						for i = 1, #chunks, 1 do

							-- Open the file for appending
							local file, errorString = open( self._datafile, "ab" )

							-- Check we have a file
							if file then

								-- Write out the current chunk
							    file:write( chunks[ i ] )

								-- Close the file handle
								close( file )

								-- Are we using chunks?
								if self._chunkSize then

									-- Yield the coroutine
									yield()

								end

							end

				        end

						-- Mark us as not saving
						self._isSaving = false

						-- Fire an event!
						self:dispatchEvent{ name = "save", phase = "ended", start = t1, finish = clock(), elapsed = self:_elapsed( t1 ) }

						-- Log the saving
						self:_log( "Data saved.", "save" )

						-- Cancel the timer
						if timerID then
							cancel( timerID )
						end

						-- Nil out the handle
						timerID = nil

					end

					-- Are we using chunks?
					if self._chunkSize then

						-- If so then create a timer and wrap the write function up as a coroutine
						timerID = performWithDelay( 1, wrap( write ), 0 )

					else

						-- Otherwise just write as normal
						write()

					end

				end

			else -- Uh oh

				self:_log( "Data could not be saved - " .. errorString, "save" )

			end

			-- Nil out the file handle
			file = nil

		else -- Uh oh

			self:_log( "Data could not be saved", "save" )

		end

	end

end

--- Loads all data.
-- @return True if loading was successful, false otherwise.
function Container:load()

	-- Ensure we aren't currently loading
	if not self:isLoading() and not self._loaded then

		-- Mark us as not loaded
		self._loaded = false

		local file, errorString

		-- Open the file for reading
		file, errorString = open( self._datafile, "rb" )

		-- Flag to mark if the file was freshly created
		local freshFile = false

		-- Was there a file?
		if not file then

			-- Open the file for writing
			file, errorString = open( self._datafile, "wb" )

			-- Close it out again
			if file then
				close( file )
			end

			-- And load it back up again for reading
			file, errorString = open( self._datafile, "rb" )

			-- Mark this file as freshly created
			freshFile = true

		end

		-- Do we have a file?
		if file then

			-- Get the current time for before the call
			local t1 = clock()

			-- Mark us as loading
			self._isLoading = true

			-- Fire an event!
			self:dispatchEvent{ name = "load", phase = "started", start = t1 }

			-- Get the contents
			local contents = file:read( "*a" )

			-- Do we have a cipher?
			if self._cipher and not self._plaintext and not self._disableEncryption then

				-- Decrypt the contents
				contents = self._cipher:decrypt( contents, self._key )

			end

			-- Binary decode the contents
			if not self._plaintext then
				contents = self:b64Decode( contents ) or ""
			end

			-- Json decode the contents
			local data = decode( contents ) or {}

			-- Do we have some data? And is the name set correctly ( i.e. it's been loaded and decrypted ) or if it was just created
			if data or freshFile then

				-- Mark us as loaded
				self._loaded = true

				self._data = data

				-- Log the loading
				self:_log( "Data loaded.", "load" )

				-- Did we use a key?
				if self._key and not self._disableEncryption then

					-- Mark us as decrypted
					self._decrypted = true

				end

			else

				-- Mark us as not loaded
				self._loaded = false

				-- Mark us as not decrypted
				self._decrypted = false

				-- We didn't decrypt anything.
				self:_log( "Data could not be loaded.", "load" )

			end

			-- Close the file handle
			close( file )

			-- Mark us as not loading
			self._isLoading = false

			-- Fire an event!
			self:dispatchEvent{ name = "load", phase = "ended", start = t1, finish = clock(), elapsed = self:_elapsed( t1 ) }


		else -- Uh oh

			-- Mark us as not loaded
			self._loaded = false

			self:_log( "Data could not be loaded - " .. ( errorString or "" ), "load" )

		end

		-- Nil out the file handle
		file = nil

		-- Return the loaded flag
		return self._loaded

	end

end

--- Wipes a data box.
-- @param box The name of the box. Optional, defaults to the global box.
function Container:wipe( box )

	-- Wipe the box
	self._data[ box or self._defaultBox ] = { data = {}, header = self._data[ box or self._defaultBox ].header }

	-- Should we now save?
	if self:isAutosaveEnabled() then
		self:save()
	end

	self:_log( ( box or self._defaultBox ) .. " wiped.", "wipe" )

end

--- Deletes all data.
function Container:delete()

	-- Wipe the in-memory version of the data
	self._data = {}

	-- Delete the file
	local result, reason = delete( self._datafile )

	-- Was the file removed?
	if result then
		self:_log( "Data deleted.", "delete" )
	else
		self:_log( "Data could not be deleted - " .. reason, "delete" )
	end

end

--- Checks if a box exists.
-- @param box The name of the box. Optional, defaults to the global box.
-- @return True if it does, false otherwise.
function Container:exists( box )
	return self._data[ box or self._defaultBox ] ~= nil
end

--- Enables autosave. This will mean every time a value is set, or a box is wiped, it will automatically save.
function Container:enableAutosave()
	self._autosaveEnabled = true
	self:_log( "Autosaving enabled.", "enableAutosave" )
end

--- Disables autosave.
function Container:disableAutosave()
	self._autosaveEnabled = false
	self:_log( "Autosaving disabled.", "disableAutosave" )
end

--- Checks if autosave is enabled.
-- @return True if it is, false otherwise.
function Container:isAutosaveEnabled()
	return self._autosaveEnabled
end

--- Enables saving on system suspend.
function Container:enableSaveOnSuspend()
	self._saveOnSuspendEnabled = true
	self:_log( "Saving on suspend enabled.", "enableSaveOnSuspend" )
end

--- Disables saving on system suspend.
function Container:disableSaveOnSuspend()
	self._saveOnSuspendEnabled = false
	self:_log( "Saving on suspend disabled.", "disableSaveOnSuspend" )
end

--- Checks if saving on system suspend is enabled.
-- @return True if it is, false otherwise.
function Container:isSavingOnSuspendEnabled()
	return self._saveOnSuspendEnabled
end

--- Enables saving on system exit.
function Container:enableSaveOnExit()
	self._saveOnExitEnabled = true
	self:_log( "Saving on exit enabled.", "enableSaveOnExit" )
end

--- Disables saving on system exit.
function Container:disableSaveOnExit()
	self._saveOnExitEnabled = false
	self:_log( "Saving on exit disabled.", "disableSaveOnExit" )
end

--- Checks if saving on system exit is enabled.
-- @return True if it is, false otherwise.
function Container:isSavingOnExitEnabled()
	return self._saveOnExitEnabled
end

--- Enables logging.
function Container:enableLog()
	self._loggingEnabled = true
	self:_log( "Logging enabled.", "enableLog" )
end

--- Disables logging.
function Container:disableLog()
	self._loggingEnabled = false
	self:_log( "Logging disabled.", "disableLog" )
end

--- Checks if logging is enabled.
-- @return True if it is, false otherwise.
function Container:isLogEnabled()
	return self._loggingEnabled
end

--- Checks if the data module is initiated.
-- @return True if it is, false otherwise.
function Container:isInitiated()
	return self._initiated
end

--- Checks if the system is currently saving.
-- @return True if it is, false otherwise.
function Container:isSaving()
	return self._isSaving
end

--- Checks if the system is currently loading.
-- @return True if it is, false otherwise.
function Container:isLoading()
	return self._isLoading
end

--- Prints a data box.
-- @param box The name of the box. Optional, defaults to the global box.
function Container:print( box )
	print( encode( self._data[ box or self._defaultBox ] ) )
end

--- Deletes the current log file.
function Container:clearLog()
	delete( self._logfile )
end

--- Gets a list of the names of all current boxes.
-- @return The list.
function Container:list()

	-- Table to store names
	local names = {}

	-- Loop through the data
	for k, _ in pairs( self._data ) do

		-- Store the name
		names[ #names + 1 ] = k

	end

	-- Return the list of names
	return names

end

--- Calculates a checksum of a box's data.
-- @param box The name of the box. Optional, defaults to the global box.
-- @param key The key to use for the hashing, optional defaults to nil for a digest hash.
-- @return The checksum.
function Container:hash( box, key )
	if self._data[ box or self._defaultBox ] then
		if self._key then
			return hmac( md5, encode( self._data[ box or self._defaultBox ].data ), self._key )
		else
			return digest( md5, encode( self._data[ box or self._defaultBox ].data ) )
		end
	end
end

--- Confirms a stored checksum matches the current one.
-- @param box The name of the box. Optional, defaults to the global box.
-- @param key The key to use for the hashing, optional defaults to nil for a digest hash.
-- @return True if it does, false otherwise.
function Container:authenticate( box, key )
	if self._data[ box or self._defaultBox ] then
		return self:hash( box, self._key ) == self._data[ box or self._defaultBox ].header.checksum
	end
end

--- Gets the current version number of the library.
-- @return The version number.
function Container:getVersion()
	return self._version
end

--- Event handler for 'system' events.
-- @param event The event table.
function Container:system( event )
	if event.type == "applicationExit" and self:isSavingOnExitEnabled() then
		self:save()
	elseif event.type == "applicationSuspend" and self:isSavingOnSuspendEnabled() then
		self:save()
	end
end

--- Binary encodes some data.
-- @param data The data to encode.
-- @return The encoded data.
function Container:b64Encode( data )
	return b64( data )
end

--- Decodes some binary encoded data.
-- @param data The data to decode.
-- @return The decoded data.
function Container:b64Decode( data )
	return unb64( data )
end

--- Adds an event listener.
-- @param name The name of the event you wish to listen for. Valid events are 'save', 'load', 'import', 'export', and 'set'.
-- @param listener Listeners can be either functions or table objects.
function Container:addEventListener( name, listener )

	-- Do we have a name?
	if name then

		-- And do we have a listener?
		if listener then

			-- Make sure we have a table ready to store this listener
			self._listeners[ name ] = self._listeners[ name ] or {}

			-- Create a new listener
			self._listeners[ name ][ #self._listeners[ name ] + 1 ] =
			{
				raw = listener, -- Storing out the raw listener object

				-- And create a callback that allows for function or table listeners
				callback = function( event )

					if type( listener ) == "function" then
						listener( event )
					elseif type( listener ) == "table" and listener[ name ] and type( listener[ name ] ) == "function" then
						listener[ name ]( listener, event )
					end

				end

			}

		end

	end

end

 --- Removes an event listener.
 -- @param name The name of the event you were listening for.
 -- @param listener Reference to the listener to remove from the list.
function Container:removeEventListener( name, listener )

	-- Do we have any listeners for this event name?
	if self._listeners[ name ] then

		-- Loop through them all in reverse order
		for i = #self._listeners[ name ], 1, -1 do

			-- Check if the raw listener for this matches the passed in one
			if self._listeners[ name ][ i ].raw == listener then

				-- If it does then remove the listener
				remove( self._listeners[ name ], i )

			end
		end

	end

end

--- Dispatches an event.
-- @param event Table containing event properties. Must contain a name property that corresponds to the event you wish to dispatch.
function Container:dispatchEvent( event )

	-- Do we have an event?
	if event then

		-- Does it have a name?
		if event.name then

			-- Do we have any listeners for this event name?
			if self._listeners[ event.name ] then

				-- Loop through all of them
				for i = 1, #self._listeners[ event.name ], 1 do

					-- And call them, passing the event table
					self._listeners[ event.name ][ i ].callback( event )

				end

			end

			-- Do we have a general listener?
			if self._listener and type( self._listener ) == "function" then

				-- Call it and pass the event table
				self._listener( event )
			end

		end

	end

end

--- Saves an entry to the log.
-- @param message The message to save.
-- @param method The name of the method we're in. Optional, defaults to nil.
function Container:_log( message, method )

	-- Is logging enabled?
	if self:isLogEnabled() then

		-- Create the base entry text
		local entry = "ScrappyData: "

		-- Was a method passed in
		if method then

			-- Append the method name
			entry = entry .. method .. "() "

		end

		-- Append the message
		entry = entry .. "- " .. message

		-- Print the message to the console
		print( entry )

		-- Open the log file for appending
		local file = open( self._logfile, "a" )

		-- Create the timestamp
		local timestamp = date( "%b %d %X", time() )

		-- Write out the new entry prefixed with the timestamp
		file:write( timestamp .. " " .. entry .. "\n" )

		-- Close the file
		close( file )

		-- Nil out the handle
		file = nil

	end

end

--- Splits some text into chunks of a certain size.
-- @param text The text to split.
-- @param size The size of the chunks.
-- @return A list of chunks.
function Container:_chunk( text, size )

	-- Do we have some text?
	if text then

		-- Do we have a chunk size?
		if size then

			-- Table to store chunks
			local chunks = {}

			-- Loop through the text in blocks
			for i = 1, #text, size do

				-- Pull out each chunk and store it
				chunks[ #chunks + 1 ] = text:sub( i, i + size - 1 )

			end

			-- Return the chunks
			return chunks

		end

	end

end

--- Calculates the elapsed time in milliseconds between a start and finish time.
-- @param t1 The start time in milliseconds.
-- @param t2 The finish time in milliseconds. Optional, defaults to now.
-- @return The elpased time.
function Container:_elapsed( t1, t2 )
	return format( "%.3f", ( ( t2 or clock() ) - t1 ) * 1000 )
end

--- Destroys this container.
function Container:destroy()

	-- Nil out the in-memory data
	self._data = nil

	-- Remove the event listener
	Runtime:removeEventListener( "system", self )

	-- Remove all event listeners.
	self._listeners = {}

end

-- Return the class
return Container
