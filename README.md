# Scrappy Data

Data storage library for Solar2D that includes a simple API as well as unlimited "boxes" and optional encryption.
